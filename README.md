# RGV Betting Bot

This is a Discord bot that allows users in the [RGV Aerial Photography Discord server](https://discord.gg/rgv) to bet on event, usually space related, outcomes.

## Running the bot

-  Install [Node.js](https://nodejs.org/en/download/)
-  `git clone https://gitlab.com/rgv-aerial/betting-bot.git`
-  `cd betting-bot`
-  `npm install`
-  `npm run dev` or `yarn dev`

# Contributing

-  Fork the repository
-  Create a new branch
-  Make your changes
-  Submit a merge request
-  Wait for approval and merge
import { Client } from 'discord.js';
import * as db from '../../utils/database';
import loadCommands from '../../handlers/commands';

const event = async(client:Client) => {
	await db.getConnection();
	await loadCommands(client);
	console.log('Logged in as ' + client.user?.tag);
};

module.exports = event;
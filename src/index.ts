import { Client, Collection, Interaction } from 'discord.js';
import { config } from 'dotenv';
import { join } from 'path';

config({ path: join(__dirname, '..', '.env') });

const client = new Client({intents: ['MessageContent', 'GuildMembers', 'GuildMessages', 'Guilds']});

client.commands = new Collection();

client.on('ready', () => {
	// eslint-disable-next-line @typescript-eslint/no-var-requires -- required for this to work
	require(join(__dirname, 'events', 'client', 'ready'))(client);
});

client.on('interactionCreate', (interaction: Interaction) => {
	// eslint-disable-next-line @typescript-eslint/no-var-requires -- required for this to work
	require(join(__dirname, 'events', 'message', 'InteractionCreate'))(client, interaction);
});

client.login(process.env.TOKEN);

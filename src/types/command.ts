import type { Interaction, Client } from 'discord.js';

export type Command = {
    name: string;
    description: string;
    default_permission: boolean;
    default_member_permissions: any;
    options: Option[];
    execute: (client: Client, interaction: Interaction) => Promise<void>;
}

export type Option = {
    name: string;
    description: string;
    type: number;
    required: boolean;
    choices?: [];
}
import { Sequelize } from 'sequelize';
import * as Bet from '../models/bet.model';

let sequelize: Sequelize;

export async function getConnection() {
	if(sequelize) {
		return sequelize;
	}

	// check if database_url is set in env, and match the regex
	const regex = /^postgres:\/\/([^:]+):([^@]+)@([^:]+):(\d+)\/(.+)$/;
	if(!process.env.DATABASE_URL || !regex.test(process.env.DATABASE_URL)) {
		throw new Error('DATABASE_URL is not defined');
	}

	// create sequelize instance
	sequelize = new Sequelize(process.env.DATABASE_URL, {
		dialect: 'postgres',
		logging: process.env?.DEBUG === 'true' ? console.log : false,
		pool: {
			// pool 
			max: 5,
			min: 0,
			acquire: 30000,
			idle: 10000
		},
		dialectOptions: {
			charset: 'utf8',
			collate: 'utf8_general_ci',
		},
	});

	// define models
	Bet.init({sequelize});

	// test connection, and throw error if failed, or return sequelize instance
	try {
		await sequelize.authenticate();
		console.log('Connection has been established successfully.');
		return sequelize;
	} catch (error) {
		console.error('Unable to connect to the database:', error);
	}
}
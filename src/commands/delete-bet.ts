import { 
	EmbedBuilder,
	PermissionFlagsBits
} from 'discord.js';
import type { Command } from '../types/command';
import { Bet } from '../models/bet.model';

const cmd: Command = {
	name: 'delete-bet',
	description: 'Delete a bet on behalf of a user.',
	default_permission: false,
	default_member_permissions: PermissionFlagsBits.ModerateMembers,
	options: [
		{
			name: 'id',
			description: 'ID of the bet to delete',
			type: 4, // integer
			required: true
		}
	],
	async execute(client, interaction) {

		if(!interaction.isCommand()) return; // so DJS stops complaining

		await Bet.destroy({
			where: {
				id: interaction.options.get('id')?.value as number
			},
		}); 

		const embed = new EmbedBuilder()
			.setTitle('Bet deleted!')
			.setDescription(`**ID**: ${interaction.options.get('id')?.value as number}`)
			.setColor('Blurple')
			.setTimestamp(new Date())
			.setFooter({text:`Bet deleted by ${interaction.user.tag}`, iconURL: interaction.user.avatarURL() ?? undefined});
        
		interaction.reply({ embeds: [embed] });
	}
};
module.exports = cmd;
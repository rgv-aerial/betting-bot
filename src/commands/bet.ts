import { 
	EmbedBuilder,
	PermissionFlagsBits
} from 'discord.js';
import type { Command } from '../types/command';
import { Bet } from '../models/bet.model';

const cmd: Command = {
	name: 'bet',
	description: 'Create a new bet',
	default_permission: true,
	default_member_permissions: PermissionFlagsBits.SendMessages,
	options: [
		{
			name: 'bet',
			description: 'What are you betting on?',
			type: 3, // string
			required: true
		},
		{
			name: 'catch',
			description: 'What are you going to do if you loose or win?',
			type: 3, // string
			required: true
		}

	],
	async execute(client, interaction) {

		if(!interaction.isCommand()) return; // so DJS stops complaining

		const newBet = await Bet.create({
			user_id: interaction.user.id,
			bet: interaction.options.get('bet')?.value as string,
			catch: interaction.options.get('catch')?.value as string,
		});

		const embed = new EmbedBuilder()
			.setTitle('Bet placed!')
			.setDescription(`**ID**: ${newBet.id}\n**Bet**: ${newBet.bet}\n**Catch**: ${newBet.catch}`)
			.setColor('Blurple')
			.setTimestamp(new Date())
			.setFooter({text:`Bet placed by ${interaction.user.tag}`, iconURL: interaction.user.avatarURL() ?? undefined});

		interaction.reply({ embeds: [embed] });
	}
};
module.exports = cmd;
import { 
	EmbedBuilder,
	ActionRowBuilder,
	ButtonBuilder,
	CollectedInteraction,
	PermissionFlagsBits
} from 'discord.js';
import type { Command } from '../types/command';
import { Bet } from '../models/bet.model';

const cmd: Command = {
	name: 'bets',
	description: 'View bets of yourself or someone else',
	default_permission: true,
	default_member_permissions: PermissionFlagsBits.SendMessages,
	options: [
		{
			name: 'user',
			description: 'The user to view bets of',
			type: 6, 
			required: false
		}
	],
	async execute(client, interaction) {

		if(!interaction.isCommand()) return; // so DJS stops complaining

		// interaction.deferReply();

		const filter = (i:CollectedInteraction) => i.isButton() && i.user.id === interaction.user.id;
		const collector = interaction.channel?.createMessageComponentCollector({ filter, time: 120 * 1000 });
		let page = 0;

		collector?.on('collect', (i) => {
			if(i.customId === 'bets-next') {
				page++;
				const { embed, component } = generate();
				i.update({ embeds: [embed], components: [component] }); // update the message
			} else if(i.customId === 'bets-previous') {
				page--;
				const { embed, component } = generate();
				i.update({ embeds: [embed], components: [component] }); // update the message
			}
		});

		const user = interaction.options.get('user')?.user?.id ? client.users.cache.get(interaction.options.get('user')?.user?.id as string) : interaction.user;

		if(!user) {
			interaction.reply('User not found... Please contact a developer if this is an error.');
			return;
		}

		const bets = await Bet.findAll({
			where: {
				user_id: user.id
			}
		});

		if(bets.length === 0) {
			interaction.editReply('No bets found for this user.');
			return;
		}
        
		const generate = () => {
			const limit = 5;
			const pages = Math.ceil(bets.length / limit);
			const start = page * limit;
			const end = start + limit;
			const _bets = bets.slice(start, end);

			const embed = new EmbedBuilder()
				.setTitle(`${user.tag} Bets`)
				.setColor('Blurple')
				.addFields(
					_bets.map(bet => ({
						name: `Bet ID: ${bet.id}`,
						value: `\n**Bet**: ${bet.bet} \
						${new Date(bet.createdAt).getTime()>new Date('2022-09-01').getTime() 
							? `\n**Catch**: ${bet.catch}` : ''} \
						\n**Submission Date**: <t:${Math.floor(new Date(bet.createdAt).getTime()*0.001)}:D>`
					}))
				);

			embed.setFooter({text:`Page ${page + 1}/${pages}`});
            
			const NextButton = new ButtonBuilder()
				.setCustomId('bets-next')
				.setEmoji('➡')
				.setLabel('Next')
				.setCustomId('bets-next')
				.setDisabled(page + 1 === pages)
				.setStyle(1);

			const PreviousButton = new ButtonBuilder()
				.setCustomId('bets-previous')
				.setEmoji('⬅')
				.setLabel('Previous')
				.setCustomId('bets-previous')
				.setDisabled(page === 0)
				.setStyle(1);

			const row = new ActionRowBuilder<ButtonBuilder>()
				.addComponents([PreviousButton, NextButton]);
            

			return {
				embed: embed,
				component: row
			};
		};

		const { embed, component } = generate();

		if(interaction.replied || interaction.deferred) {
			interaction.editReply({ embeds: [embed], components: [component] });
		} else {
			interaction.reply({ embeds: [embed], components: [component] });
		}
	}
};
module.exports = cmd;
import { 
	EmbedBuilder
} from 'discord.js';
import type { Command } from '../types/command';
import { Bet } from '../models/bet.model';

const cmd: Command = {
	name: 'get-bet',
	description: 'Get a specific bet',
	default_permission: true,
	default_member_permissions: 0x0,
	options: [
		{
			name: 'id',
			description: 'ID of the bet to get',
			type: 4, // integer
			required: true
		}
	],
	async execute(client, interaction) {

		if(!interaction.isCommand()) return; // so DJS stops complaining

		const bet = await Bet.findOne({
			where: {
				id: interaction.options.get('id')?.value as number
			}
		});
		if (bet != null) {
			const embed = new EmbedBuilder()
				.setTitle('Bet' + bet.id)
				.setDescription(`\n**Bet**: ${bet.bet} \
            ${new Date(bet.createdAt).getTime()>new Date('2022-09-01').getTime() ? `\n**Catch**: ${bet.catch}` : ''} \
            \n**Submission Date**: <t:${Math.floor(new Date(bet.createdAt).getTime()*0.001)}:D>`)
				.setColor('Blurple')
				.setTimestamp(new Date())
				.setFooter({text:`Bet placed by ${client.users.cache.find(user => user.id === bet.user_id)?.tag}`, iconURL: client.users.cache.find(user => user.id === bet.user_id)?.avatar ?? undefined});
			interaction.reply({ embeds: [embed] });
		}
		else {
			interaction.reply({ content: 'Bet not found', ephemeral: true });
		}			
	}
};
module.exports = cmd;

import type { Command } from '../types/command';
import { PermissionFlagsBits } from 'discord.js';


const cmd: Command = {
	name: 'add-to-thread',
	description: 'Add members of role to the current thread.',
	default_permission: false,
	default_member_permissions: PermissionFlagsBits.ModerateMembers,
	options: [],
	async execute(client, interaction) {
		const max_pings_per_message = 94;

		if(!interaction.isCommand()) return; // so DJS stops complaining

		const thread = interaction.channel;
		if(!thread?.isThread()) {
			interaction.reply({ ephemeral: true, content: 'This command can only be used in threads.' });
			return;
		}
		const role_id = process.env.ADD_TO_THREAD_ROLE_ID;
		if (!role_id) {
			interaction.reply({ ephemeral: true, content: 'No role ID found.' });
			return;
		}
		const role = client.guilds.cache.get(interaction.guildId as string)?.roles.cache.get(role_id);
		if(role === undefined) { 
			interaction.reply({ ephemeral: true, content: 'Role ID invalid. Please contact a developper to fix this issue.' });
			return;
		}
		const role_members = role.members;
		if(role_members.size === 0) {
			interaction.reply({ ephemeral: true, content: 'No members found in the role.' });
			return;
		}

		const webhook = await thread.parent?.createWebhook({ name: 'add-to-thread', avatar: client.user?.avatarURL() });
        
		const messages = [];

		const message_amount = Math.ceil(role_members.size / max_pings_per_message);
		for (let i = 0; i < message_amount; i++) {

			const start = i * max_pings_per_message;
			const end = start + max_pings_per_message;

			const members_to_ping = role_members.map(m => m.id).slice(start, end);

			const member_string = '<@' + members_to_ping.join('> <@') + '>';

			messages.push(await webhook?.send({content: member_string, threadId: thread.id}));
		}
		messages.forEach(async (message) => { message?.delete(); });

		webhook?.delete();
		interaction.reply({ ephemeral: true, content: 'Done!' });
	}
};
module.exports = cmd;

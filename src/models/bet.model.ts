import { Model, Sequelize, DataTypes } from 'sequelize';

type BetAttributes = {
    id: number,
    user_id: string,
    bet: string,
	catch: string,
    createdAt: Date,
    updatedAt: Date,
};

type BetCreationAttributes = Pick<BetAttributes, 'user_id' | 'bet'| 'catch' >;

export class Bet extends Model<BetAttributes,BetCreationAttributes> {
	declare id: number;
	declare user_id: string;
	declare bet: string;
	declare catch: string;
	declare createdAt: Date;
	declare updatedAt: Date;
}

export function init( { sequelize }: { sequelize: Sequelize } ) {
	Bet.init({
		id: {
			type: DataTypes.INTEGER,
			primaryKey: true,
			autoIncrement: true
		},
		user_id: {
			type: DataTypes.STRING,
			allowNull: false
		},
		bet: {
			type: DataTypes.STRING,
			allowNull: false
		},
		catch: {
			type: DataTypes.STRING,
			allowNull: true
		},
		createdAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: Sequelize.fn('now')
		},
		updatedAt: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: Sequelize.fn('now')
		}
	}, {
		sequelize,
		tableName: 'bets',
		charset: 'utf8',
		collate: 'utf8_general_ci'
	});
}

export default Bet;